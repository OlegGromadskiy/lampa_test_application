import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/res/app_colors.dart';
import 'package:lamp_company_tech_task/shared_widgets/replacement.dart';

class CheckboxIcon extends StatefulWidget {
  final double size;
  final String? title;
  final bool isChecked;
  final Color checkedColor;
  final Color uncheckedColor;
  final IconData checkedIcon;
  final IconData uncheckedIcon;
  final ValueChanged<bool>? onChanged;

  const CheckboxIcon({
    required this.checkedIcon,
    required this.uncheckedIcon,
    this.checkedColor = AppColors.grey,
    this.uncheckedColor = AppColors.grey,
    this.title,
    this.size = 32.0,
    this.isChecked = false,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  _CheckboxIconState createState() => _CheckboxIconState();
}

class _CheckboxIconState extends State<CheckboxIcon> with SingleTickerProviderStateMixin {
  late final isCheckedNotifier = ValueNotifier<bool>(widget.isChecked);
  late final controller = AnimationController(vsync: this, duration: Duration(milliseconds: 100));

  @override
  Widget build(BuildContext context) {
    Widget result = SizedBox(
      height: widget.size,
      width: widget.size,
      child: AnimatedBuilder(
        animation: controller,
        builder: (context, child) {
          return Center(
            child: SizedBox(
              height: widget.size - 2.0 * (1 - controller.value),
              width: widget.size - 2.0 * (1 - controller.value),
              child: child,
            ),
          );
        },
        child: Material(
          color: AppColors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.circular(360.0),
            onTap: () {
              controller.forward().then((value) => controller.reverse());
              isCheckedNotifier.value = !isCheckedNotifier.value;
              widget.onChanged?.call(isCheckedNotifier.value);
            },
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: ValueListenableBuilder<bool>(
                valueListenable: isCheckedNotifier,
                builder: (context, value, child) => Replacement(
                  needReplace: isCheckedNotifier.value,
                  defaultBuilder: (_) => FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      widget.uncheckedIcon,
                      color: widget.uncheckedColor,
                    ),
                  ),
                  replacementBuilder: (_) => FittedBox(
                    fit: BoxFit.fill,
                    child: Icon(
                      widget.checkedIcon,
                      color: widget.checkedColor,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );

    if (widget.title != null) {
      result = Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          result,
          Text(widget.title!),
        ],
      );
    }

    return result;
  }
}
