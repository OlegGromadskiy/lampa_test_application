import 'package:flutter/material.dart';

class Replacement extends StatelessWidget {
  final bool needReplace;
  final WidgetBuilder defaultBuilder;
  final WidgetBuilder replacementBuilder;

  const Replacement({
    required this.needReplace,
    required this.defaultBuilder,
    required this.replacementBuilder,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (needReplace) {
      return replacementBuilder(context);
    }
    return defaultBuilder(context);
  }
}
