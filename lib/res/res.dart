export 'package:lamp_company_tech_task/res/app_colors.dart';
export 'package:lamp_company_tech_task/res/app_assets.dart';
export 'package:lamp_company_tech_task/res/app_text_styles.dart';
export 'package:lamp_company_tech_task/res/app_consts.dart';
export 'package:lamp_company_tech_task/res/app_locales.dart';
export 'package:flutter_gen/gen_l10n/app_localizations.dart';