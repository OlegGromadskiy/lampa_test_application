import 'package:flutter/material.dart';

abstract class AppColors{
  AppColors._();

  static const Color blue = Color(0xFF20B3C5);
  static const Color white = Color(0xFFFFFFFF);
  static const Color grey = Color(0xFFA2A2A2);
  static const Color lightGrey = Color(0xFFDDDDDD);
  static const Color black = Color(0xFF000000);
  static const Color lipstick = Color(0xFFFF5872);
  static const Color transparent = Color(0x00FFFFFF);
  static const Color orange = Color(0xFFED4433);
}