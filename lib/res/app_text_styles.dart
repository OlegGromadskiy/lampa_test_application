import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lamp_company_tech_task/res/app_colors.dart';

abstract class AppTextStyles {
  AppTextStyles._();

  static TextStyle robotoRegular = GoogleFonts.roboto(
    color: AppColors.black,
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
  );

  static TextStyle robotoLight = GoogleFonts.roboto(
    color: AppColors.black,
    fontSize: 12.0,
    fontWeight: FontWeight.w300,
  );

  static TextStyle robotoBold = GoogleFonts.roboto(
    color: AppColors.black,
    fontSize: 12.0,
    fontWeight: FontWeight.w700,
  );
}
