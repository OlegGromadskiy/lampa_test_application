abstract class DateFormats{
  DateFormats._();

  ///ddMMMyyyy
  static const String dayMonthNameYear = 'dd MMMM yyyy';
}