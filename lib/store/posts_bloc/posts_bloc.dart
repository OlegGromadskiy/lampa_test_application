import 'package:bloc/bloc.dart';
import 'package:lamp_company_tech_task/bloc/bloc_interfaces.dart';
import 'package:lamp_company_tech_task/network/repos/posts_repository.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_events.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_state.dart';

class PostsBloc extends Bloc<IEvent, PostsState> {
  PostsBloc() : super(PostsState()) {
    on<FetchPostsEvent>(_fetchPosts);
    on<FetchTopicOfTheDayEvent>(_fetchTopic);
    on<ChangeLoadingStateEvent>(_changeLoadingState);
  }

  Future<void> _fetchPosts(FetchPostsEvent event, Emitter emit) async {
    final posts = await PostsRepository.fetchPosts(0, 0);

    if (state.posts == null) {
      emit(state.copyWith(posts: posts));
    } else {
      emit(state.copyWith(posts: [...state.posts!, ...posts]));
    }

    if(state.isLoading){
      await Future.delayed(Duration(milliseconds: 333));
      emit(state.copyWith(isLoading: false));
    }
  }

  Future<void> _fetchTopic(FetchTopicOfTheDayEvent event, Emitter emit) async {
    final topic = await PostsRepository.fetchTopic();

    emit(state.copyWith(topicOfTheDay: topic));
  }

  Future<void> _changeLoadingState(ChangeLoadingStateEvent event, Emitter emit) async {
    emit(state.copyWith(isLoading: event.isLoading));
  }
}
