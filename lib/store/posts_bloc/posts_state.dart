import 'package:lamp_company_tech_task/bloc/bloc_interfaces.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';

class PostsState extends IState {
  final bool isLoading;
  final List<PostDto>? posts;
  final TopicOfTheDayDto? topicOfTheDay;

  PostsState({
    this.posts,
    this.isLoading = false,
    this.topicOfTheDay,
  });

  PostsState copyWith({
    bool? isLoading,
    List<PostDto>? posts,
    TopicOfTheDayDto? topicOfTheDay,
  }) {
    return PostsState(
      posts: posts ?? this.posts,
      isLoading: isLoading ?? this.isLoading,
      topicOfTheDay: topicOfTheDay ?? this.topicOfTheDay,
    );
  }
}
