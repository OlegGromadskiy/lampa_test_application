import 'package:lamp_company_tech_task/bloc/bloc_interfaces.dart';

class FetchPostsEvent extends IEvent{}
class FetchTopicOfTheDayEvent extends IEvent{}
class ChangeLoadingStateEvent extends IEvent{
  final bool isLoading;

  ChangeLoadingStateEvent(this.isLoading);
}