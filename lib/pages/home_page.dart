import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/bloc/bloc_widgets.dart';
import 'package:lamp_company_tech_task/pages/widgets/views/popular_posts_view/popular_posts_view.dart';
import 'package:lamp_company_tech_task/pages/widgets/views/subscriptions_view/subscriptions_view.dart';
import 'package:lamp_company_tech_task/res/res.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_bloc.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_events.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_state.dart';
import 'widgets/views/new_posts_view/new_posts_view.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: buildAppBar(context),
        body: BlocHolder<PostsState>(
          bloc: PostsBloc()..add(FetchTopicOfTheDayEvent())..add(FetchPostsEvent()),
          child: TabBarView(
            children: [
              NewPostsView(),
              PopularPostsView(),
              SubscriptionsView(),
            ],
          ),
        ),
      ),
    );
  }

  PreferredSizeWidget buildAppBar(BuildContext context) {
    final locale = AppLocalizations.of(context)!;

    return AppBar(
      title: Text(
        locale.news,
        style: AppTextStyles.robotoBold.copyWith(fontSize: 24.0),
      ),
      leading: IconButton(
        splashRadius: 24.0,
        icon: DecoratedBox(
          decoration: BoxDecoration(
            color: AppColors.blue,
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Icon(Icons.menu),
          ),
        ),
        color: AppColors.white,
        onPressed: () {},
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.location_on_outlined),
          splashRadius: 24.0,
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(Icons.search),
          splashRadius: 24.0,
          onPressed: () {},
        ),
      ],
      titleSpacing: 12.0,
      bottom: TabBar(
        indicatorWeight: 4.0,
        indicatorColor: AppColors.blue,
        unselectedLabelColor: AppColors.grey,
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
        tabs: [
          Tab(text: locale.new_word),
          Tab(text: locale.popular),
          Tab(text: locale.subscriptions),
        ],
      ),
    );
  }
}
