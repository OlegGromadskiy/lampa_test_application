import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/res/res.dart';

class SubscriptionsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context)!;

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: DefaultTextStyle(
        style: AppTextStyles.robotoBold.copyWith(
          color: AppColors.white,
          fontSize: 38.0,
        ),
        child: Center(
          child: Text(
            locale.anySubscriptions,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
