import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/bloc/bloc_widgets.dart';
import 'package:lamp_company_tech_task/pages/widgets/post_widget.dart';
import 'package:lamp_company_tech_task/pages/widgets/posts_list.dart';
import 'package:lamp_company_tech_task/pages/widgets/views/new_posts_view/new_posts_vm.dart';
import 'package:lamp_company_tech_task/pages/widgets/views/topic_of_the_day_widget.dart';

class NewPostsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<NewPostsViewModel>(
      converter: (context) => NewPostsViewModel(context),
      builder: (context, vm) => PostsList(
        builder: (_, index) {
          if (index == 0) {
            return TopicOfTheDayWidget(topicOfTheDay: vm.topicOfTheDay);
          }

          if (index % 2 != 0) {
            return const SizedBox(height: 8.0);
          } else {
            int localIndex = index ~/ 2;


            return PostWidget(postDto: vm.posts![localIndex - 1]);
          }
        },
        canFetch: !vm.isLoading,
        itemCount: (vm.posts?.length ?? 0) * 2 + 2,
        needLoader: vm.posts == null,
      ),
    );
  }
}
