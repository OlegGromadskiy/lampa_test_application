import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/entities/view_model.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_state.dart';

import 'package:darq/darq.dart';
class NewPostsViewModel extends IViewModel{
  late final List<PostDto>? posts;
  late final TopicOfTheDayDto? topicOfTheDay;
  late final bool isLoading;

  NewPostsViewModel(BuildContext context) : super(context){
    posts = watch<PostsState>().posts?.orderByDescending((post) => post.postDate?.millisecondsSinceEpoch ?? 0).toList();
    isLoading = watch<PostsState>().isLoading;
    topicOfTheDay = watch<PostsState>().topicOfTheDay;
  }
}