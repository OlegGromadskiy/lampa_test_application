import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';
import 'package:lamp_company_tech_task/res/res.dart';
import 'package:lamp_company_tech_task/shared_widgets/shared_widgets.dart';

class TopicOfTheDayWidget extends StatefulWidget {
  final TopicOfTheDayDto? topicOfTheDay;

  const TopicOfTheDayWidget({
    required this.topicOfTheDay,
    Key? key,
  }) : super(key: key);

  @override
  _TopicOfTheDayWidgetState createState() => _TopicOfTheDayWidgetState();
}

class _TopicOfTheDayWidgetState extends State<TopicOfTheDayWidget> with SingleTickerProviderStateMixin {
  bool isOpened = true;

  @override
  Widget build(BuildContext context) {
    final locale = AppLocalizations.of(context)!;

    return AnimatedSize(
      vsync: this,
      duration: Duration(milliseconds: 200),
      child: Replacement(
        needReplace: widget.topicOfTheDay?.content == null || !isOpened,
        replacementBuilder: (context) => const SizedBox(),
        defaultBuilder: (context) => Column(
          children: [
            const SizedBox(height: 8.0),
            Container(
              height: 130.0,
              color: AppColors.blue,
              padding: const EdgeInsets.only(left: 12.0, bottom: 12.0, right: 12.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.arrow_right_alt_rounded,
                        color: AppColors.white,
                      ),
                      const SizedBox(width: 4.0),
                      Text(
                        locale.topic_of_the_day,
                        style: AppTextStyles.robotoLight.copyWith(
                          color: AppColors.white,
                          fontSize: 12.0,
                        ),
                      ),
                      Spacer(),
                      Transform.translate(
                        offset: Offset(12.0, 0.0),
                        child: Material(
                          color: AppColors.transparent,
                          child: IconButton(
                            onPressed: () => setState(() => isOpened = false),
                            splashRadius: 18.0,
                            splashColor: AppColors.white.withOpacity(0.1),
                            highlightColor: AppColors.white.withOpacity(0.1),
                            icon: Icon(
                              Icons.close_rounded,
                              color: AppColors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    widget.topicOfTheDay!.content!,
                    style: AppTextStyles.robotoBold.copyWith(
                      color: AppColors.white,
                      fontSize: 26.0,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
