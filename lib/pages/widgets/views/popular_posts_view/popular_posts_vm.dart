import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/entities/view_model.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_state.dart';

import 'package:darq/darq.dart';

class PopularPostsViewModel extends IViewModel{
  late final List<PostDto>? posts;
  late final bool isLoading;

  PopularPostsViewModel(BuildContext context) : super(context){
    posts = watch<PostsState>().posts?.orderByDescending((post) => post.likes ?? 0).thenByDescending((post) => post.comments ?? 0).toList();
    isLoading = watch<PostsState>().isLoading;
  }
}