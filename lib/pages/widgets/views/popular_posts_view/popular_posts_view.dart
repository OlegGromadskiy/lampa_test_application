import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/bloc/bloc_widgets.dart';
import 'package:lamp_company_tech_task/pages/widgets/post_widget.dart';
import 'package:lamp_company_tech_task/pages/widgets/posts_list.dart';
import 'package:lamp_company_tech_task/pages/widgets/views/popular_posts_view/popular_posts_vm.dart';

class PopularPostsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<PopularPostsViewModel>(
      converter: (context) => PopularPostsViewModel(context),
      builder: (context, vm) => PostsList(
        builder: (_, index) {
          if (index % 2 == 0) {
            return const SizedBox(height: 8.0);
          } else {
            return PostWidget(postDto: vm.posts![index ~/2]);
          }
        },
        canFetch: !vm.isLoading,
        itemCount: (vm.posts?.length ?? 0) * 2 + 1,
        needLoader: vm.posts == null,
      ),
    );
  }
}
