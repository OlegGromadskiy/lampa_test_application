import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/pages/widgets/decorated_text.dart';
import 'package:lamp_company_tech_task/res/app_text_styles.dart';
import 'package:lamp_company_tech_task/res/res.dart';
import 'package:lamp_company_tech_task/shared_widgets/checkbox_icon.dart';
import 'package:lamp_company_tech_task/shared_widgets/replacement.dart';
import 'package:lamp_company_tech_task/utils/date_formats.dart';

class PostWidget extends StatefulWidget {
  final PostDto postDto;

  const PostWidget({
    required this.postDto,
    Key? key,
  }) : super(key: key);

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  final controller = TransformationController();
  late final locale = AppLocalizations.of(context)!;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 24.0,
                  child: Row(
                    children: [
                      Replacement(
                        needReplace: widget.postDto.userInfo?.imageUrl == null ||
                            widget.postDto.userInfo == null ||
                            widget.postDto.userInfo!.imageUrl!.isEmpty,
                        replacementBuilder: (_) => Image.asset(AppAssets.avatarWoman),
                        defaultBuilder: (_) => ClipOval(
                          child: CachedNetworkImage(
                            imageUrl: widget.postDto.userInfo!.imageUrl!,
                            errorWidget: (context, url, error) => Image.asset(AppAssets.avatarWoman),
                            progressIndicatorBuilder: (context, url, progress) => Image.asset(AppAssets.avatarWoman),
                            height: 24.0,
                            width: 24.0,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      Text(
                        name,
                        style: AppTextStyles.robotoBold,
                      ),
                      if (yearsAndMonths != null)
                        DecoratedText(
                          text: yearsAndMonths!,
                          color: AppColors.lipstick,
                          margin: const EdgeInsets.symmetric(horizontal: 4.0),
                        ),
                      if (formattedWeeks != null)
                        DecoratedText(
                          text: formattedWeeks!,
                          color: AppColors.blue,
                        ),
                    ],
                  ),
                ),
                const SizedBox(height: 12.0),
                if (widget.postDto.attachedImageUrl != null)
                  Column(
                    children: [
                      InteractiveViewer(
                        transformationController: controller,
                        onInteractionEnd: (details) {
                          controller.value = Matrix4.identity();
                        },
                        child: CachedNetworkImage(
                          imageUrl: widget.postDto.attachedImageUrl!,
                          progressIndicatorBuilder: (context, url, progress) => SizedBox(
                            height: 200.0,
                            child: Center(child: CircularProgressIndicator()),
                          ),
                          errorWidget: (context, url, error) => SizedBox(
                            height: 200.0,
                            child: Center(child: CircularProgressIndicator()),
                          ),
                        ),
                      ),
                      const SizedBox(height: 4.0),
                    ],
                  ),
                Text(
                  widget.postDto.title ?? AppConsts.emptyString,
                  style: AppTextStyles.robotoBold.copyWith(fontSize: 24.0),
                ),
                const SizedBox(height: 24.0),
                Text(widget.postDto.content ?? AppConsts.emptyString),
                const SizedBox(height: 24.0),
                DefaultTextStyle(
                  style: AppTextStyles.robotoRegular.copyWith(color: AppColors.grey),
                  child: Row(
                    children: [
                      if (widget.postDto.categoryName != null) Text(date),
                      const Spacer(),
                      if (widget.postDto.categoryName != null)
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                              Icons.arrow_right_alt_rounded,
                              color: AppColors.grey,
                            ),
                            Text(widget.postDto.categoryName!),
                          ],
                        )
                    ],
                  ),
                ),
                Container(
                  color: AppColors.lightGrey,
                  height: 1.0,
                ),
              ],
            ),
          ),
          DefaultTextStyle(
            style: AppTextStyles.robotoRegular.copyWith(color: AppColors.grey),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
              child: Row(
                children: [
                  CheckboxIcon(
                    title: widget.postDto.likes?.toString(),
                    isChecked: widget.postDto.isLiked ?? false,
                    checkedIcon: Icons.favorite_rounded,
                    uncheckedIcon: Icons.favorite_border_rounded,
                    checkedColor: AppColors.lipstick,
                  ),
                  CheckboxIcon(
                    title: widget.postDto.comments?.toString(),
                    isChecked: widget.postDto.isCommented ?? false,
                    checkedIcon: Icons.mode_comment_rounded,
                    uncheckedIcon: Icons.mode_comment_outlined,
                    checkedColor: AppColors.blue,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Material(
                      color: AppColors.transparent,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(360.0),
                        onTap: () {},
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.share_rounded,
                            color: AppColors.grey,
                            size: 22.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const Spacer(),
                  CheckboxIcon(
                    isChecked: widget.postDto.isFavourite ?? false,
                    checkedIcon: Icons.star_rounded,
                    uncheckedIcon: Icons.star_border_rounded,
                    checkedColor: AppColors.orange,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  String get date {
    String result = AppConsts.emptyString;

    if (widget.postDto.postDate != null) {
      result += DateFormat(
        DateFormats.dayMonthNameYear,
        AppLocalizations.of(context)!.localeName,
      ).format(widget.postDto.postDate!);
    }

    return result;
  }

  String get name {
    String result = AppConsts.emptyString;

    if (widget.postDto.userInfo != null) {
      if (widget.postDto.userInfo!.firstName != null) {
        result += widget.postDto.userInfo!.firstName!;
      }

      result += AppConsts.space;

      if (widget.postDto.userInfo!.lastName != null) {
        result += widget.postDto.userInfo!.lastName!;
      }
    }

    return result;
  }

  String? get yearsAndMonths {
    String result = AppConsts.emptyString;

    if (widget.postDto.userInfo != null) {
      if (widget.postDto.userInfo!.childYears != null) {
        result += '${widget.postDto.userInfo!.childYears}${locale.year_short}';
      }

      result += AppConsts.space;

      if (widget.postDto.userInfo!.childMonths != null) {
        result += '${widget.postDto.userInfo!.childMonths}${locale.month_short}';
      }
    }

    return result == AppConsts.space ? null : result;
  }

  String? get formattedWeeks {
    String result = AppConsts.emptyString;

    if (widget.postDto.userInfo != null && widget.postDto.userInfo!.childWeeks != null) {
      result += '${widget.postDto.userInfo!.childWeeks}${locale.week_short}';
    }

    return result == AppConsts.emptyString ? null : result;
  }
}
