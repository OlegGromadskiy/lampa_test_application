import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/bloc/context_extension.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';
import 'package:lamp_company_tech_task/shared_widgets/shared_widgets.dart';
import 'package:lamp_company_tech_task/store/posts_bloc/posts_events.dart';

class PostsList extends StatefulWidget {
  final bool canFetch;
  final int? itemCount;
  final bool needLoader;
  final IndexedWidgetBuilder builder;

  const PostsList({
    required this.builder,
    required this.canFetch,
    required this.itemCount,
    required this.needLoader,
    Key? key,
  }) : super(key: key);

  @override
  _PostsListState createState() => _PostsListState();
}

class _PostsListState extends State<PostsList> {
  final scrollController = ScrollController();
  late final VoidCallback scrollListener;

  @override
  void initState() {
    super.initState();

    scrollController.addListener(
      scrollListener = () {
        if (scrollController.position.pixels >= scrollController.position.maxScrollExtent && widget.canFetch) {
          context.dispatch(ChangeLoadingStateEvent(true));
          context.dispatch(FetchPostsEvent());
        }
      },
    );
  }

  @override
  void dispose() {
    scrollController.removeListener(scrollListener);
    scrollController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Replacement(
      needReplace: widget.needLoader,
      replacementBuilder: (context) => Center(
        child: CircularProgressIndicator(),
      ),
      defaultBuilder: (context) => CustomScrollView(
        controller: scrollController,
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
              widget.builder,
              childCount: widget.itemCount,
            ),
          ),
          const SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Center(
              child: SizedBox(
                height: 50.0,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
