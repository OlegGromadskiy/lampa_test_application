import 'package:flutter/material.dart';
import 'package:lamp_company_tech_task/res/app_colors.dart';
import 'package:lamp_company_tech_task/res/app_text_styles.dart';

class DecoratedText extends StatelessWidget {
  final Color color;
  final String text;
  final EdgeInsets margin;

  const DecoratedText({
    required this.text,
    required this.color,
    this.margin = EdgeInsets.zero,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: const EdgeInsets.symmetric(
        horizontal: 4.0,
        vertical: 2.0,
      ),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(360.0),
      ),
      child: Text(
        text,
        style: AppTextStyles.robotoLight.copyWith(color: AppColors.white),
      ),
    );
  }
}
