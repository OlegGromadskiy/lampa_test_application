import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lamp_company_tech_task/application/application.dart';

void main(){
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));

  runApp(MyApp());
}
