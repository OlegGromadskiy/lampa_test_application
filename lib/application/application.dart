import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lamp_company_tech_task/pages/home_page.dart';
import 'package:lamp_company_tech_task/res/res.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lampa test application',
      debugShowCheckedModeBanner: false,
      locale: Locale(AppLocales.ru),
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        Locale(AppLocales.en),
        Locale(AppLocales.ru),
      ],
      theme: ThemeData(
        primaryColor: AppColors.blue,
        colorScheme: ColorScheme.light(
          primary: AppColors.blue,
          secondary: AppColors.blue,
        ),
        scaffoldBackgroundColor: AppColors.lightGrey,
        splashColor: AppColors.blue.withOpacity(0.3),
        highlightColor: AppColors.blue.withOpacity(0.1),
        appBarTheme: AppBarTheme(
          backgroundColor: AppColors.white,
          textTheme: GoogleFonts.robotoTextTheme()
        ),
      ),
      home: HomePage(),
    );
  }
}
