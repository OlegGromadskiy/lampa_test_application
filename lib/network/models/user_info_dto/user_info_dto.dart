import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_info_dto.freezed.dart';

part 'user_info_dto.g.dart';

@freezed
class UserInfoDto with _$UserInfoDto {
  @JsonSerializable(
    checked: true,
    explicitToJson: true,
  )
  factory UserInfoDto({
    @JsonKey(name: 'id') String? id,
    @JsonKey(name: 'firstName') String? firstName,
    @JsonKey(name: 'lastName') String? lastName,
    @JsonKey(name: 'childYears') int? childYears,
    @JsonKey(name: 'childMonths') int? childMonths,
    @JsonKey(name: 'childWeeks') int? childWeeks,
    @JsonKey(name: 'imageUrl') String? imageUrl,
  }) = _UserInfoDto;

  factory UserInfoDto.fromJson(Map<String, dynamic> json) => _$UserInfoDtoFromJson(json);
}
