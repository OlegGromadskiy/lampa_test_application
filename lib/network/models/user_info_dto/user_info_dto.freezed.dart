// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'user_info_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserInfoDto _$UserInfoDtoFromJson(Map<String, dynamic> json) {
  return _UserInfoDto.fromJson(json);
}

/// @nodoc
class _$UserInfoDtoTearOff {
  const _$UserInfoDtoTearOff();

  _UserInfoDto call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'firstName') String? firstName,
      @JsonKey(name: 'lastName') String? lastName,
      @JsonKey(name: 'childYears') int? childYears,
      @JsonKey(name: 'childMonths') int? childMonths,
      @JsonKey(name: 'childWeeks') int? childWeeks,
      @JsonKey(name: 'imageUrl') String? imageUrl}) {
    return _UserInfoDto(
      id: id,
      firstName: firstName,
      lastName: lastName,
      childYears: childYears,
      childMonths: childMonths,
      childWeeks: childWeeks,
      imageUrl: imageUrl,
    );
  }

  UserInfoDto fromJson(Map<String, Object> json) {
    return UserInfoDto.fromJson(json);
  }
}

/// @nodoc
const $UserInfoDto = _$UserInfoDtoTearOff();

/// @nodoc
mixin _$UserInfoDto {
  @JsonKey(name: 'id')
  String? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'firstName')
  String? get firstName => throw _privateConstructorUsedError;
  @JsonKey(name: 'lastName')
  String? get lastName => throw _privateConstructorUsedError;
  @JsonKey(name: 'childYears')
  int? get childYears => throw _privateConstructorUsedError;
  @JsonKey(name: 'childMonths')
  int? get childMonths => throw _privateConstructorUsedError;
  @JsonKey(name: 'childWeeks')
  int? get childWeeks => throw _privateConstructorUsedError;
  @JsonKey(name: 'imageUrl')
  String? get imageUrl => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserInfoDtoCopyWith<UserInfoDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfoDtoCopyWith<$Res> {
  factory $UserInfoDtoCopyWith(
          UserInfoDto value, $Res Function(UserInfoDto) then) =
      _$UserInfoDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'firstName') String? firstName,
      @JsonKey(name: 'lastName') String? lastName,
      @JsonKey(name: 'childYears') int? childYears,
      @JsonKey(name: 'childMonths') int? childMonths,
      @JsonKey(name: 'childWeeks') int? childWeeks,
      @JsonKey(name: 'imageUrl') String? imageUrl});
}

/// @nodoc
class _$UserInfoDtoCopyWithImpl<$Res> implements $UserInfoDtoCopyWith<$Res> {
  _$UserInfoDtoCopyWithImpl(this._value, this._then);

  final UserInfoDto _value;
  // ignore: unused_field
  final $Res Function(UserInfoDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? childYears = freezed,
    Object? childMonths = freezed,
    Object? childWeeks = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      childYears: childYears == freezed
          ? _value.childYears
          : childYears // ignore: cast_nullable_to_non_nullable
              as int?,
      childMonths: childMonths == freezed
          ? _value.childMonths
          : childMonths // ignore: cast_nullable_to_non_nullable
              as int?,
      childWeeks: childWeeks == freezed
          ? _value.childWeeks
          : childWeeks // ignore: cast_nullable_to_non_nullable
              as int?,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$UserInfoDtoCopyWith<$Res>
    implements $UserInfoDtoCopyWith<$Res> {
  factory _$UserInfoDtoCopyWith(
          _UserInfoDto value, $Res Function(_UserInfoDto) then) =
      __$UserInfoDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'firstName') String? firstName,
      @JsonKey(name: 'lastName') String? lastName,
      @JsonKey(name: 'childYears') int? childYears,
      @JsonKey(name: 'childMonths') int? childMonths,
      @JsonKey(name: 'childWeeks') int? childWeeks,
      @JsonKey(name: 'imageUrl') String? imageUrl});
}

/// @nodoc
class __$UserInfoDtoCopyWithImpl<$Res> extends _$UserInfoDtoCopyWithImpl<$Res>
    implements _$UserInfoDtoCopyWith<$Res> {
  __$UserInfoDtoCopyWithImpl(
      _UserInfoDto _value, $Res Function(_UserInfoDto) _then)
      : super(_value, (v) => _then(v as _UserInfoDto));

  @override
  _UserInfoDto get _value => super._value as _UserInfoDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? childYears = freezed,
    Object? childMonths = freezed,
    Object? childWeeks = freezed,
    Object? imageUrl = freezed,
  }) {
    return _then(_UserInfoDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      childYears: childYears == freezed
          ? _value.childYears
          : childYears // ignore: cast_nullable_to_non_nullable
              as int?,
      childMonths: childMonths == freezed
          ? _value.childMonths
          : childMonths // ignore: cast_nullable_to_non_nullable
              as int?,
      childWeeks: childWeeks == freezed
          ? _value.childWeeks
          : childWeeks // ignore: cast_nullable_to_non_nullable
              as int?,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(checked: true, explicitToJson: true)
class _$_UserInfoDto implements _UserInfoDto {
  _$_UserInfoDto(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'firstName') this.firstName,
      @JsonKey(name: 'lastName') this.lastName,
      @JsonKey(name: 'childYears') this.childYears,
      @JsonKey(name: 'childMonths') this.childMonths,
      @JsonKey(name: 'childWeeks') this.childWeeks,
      @JsonKey(name: 'imageUrl') this.imageUrl});

  factory _$_UserInfoDto.fromJson(Map<String, dynamic> json) =>
      _$_$_UserInfoDtoFromJson(json);

  @override
  @JsonKey(name: 'id')
  final String? id;
  @override
  @JsonKey(name: 'firstName')
  final String? firstName;
  @override
  @JsonKey(name: 'lastName')
  final String? lastName;
  @override
  @JsonKey(name: 'childYears')
  final int? childYears;
  @override
  @JsonKey(name: 'childMonths')
  final int? childMonths;
  @override
  @JsonKey(name: 'childWeeks')
  final int? childWeeks;
  @override
  @JsonKey(name: 'imageUrl')
  final String? imageUrl;

  @override
  String toString() {
    return 'UserInfoDto(id: $id, firstName: $firstName, lastName: $lastName, childYears: $childYears, childMonths: $childMonths, childWeeks: $childWeeks, imageUrl: $imageUrl)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _UserInfoDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.firstName, firstName) ||
                const DeepCollectionEquality()
                    .equals(other.firstName, firstName)) &&
            (identical(other.lastName, lastName) ||
                const DeepCollectionEquality()
                    .equals(other.lastName, lastName)) &&
            (identical(other.childYears, childYears) ||
                const DeepCollectionEquality()
                    .equals(other.childYears, childYears)) &&
            (identical(other.childMonths, childMonths) ||
                const DeepCollectionEquality()
                    .equals(other.childMonths, childMonths)) &&
            (identical(other.childWeeks, childWeeks) ||
                const DeepCollectionEquality()
                    .equals(other.childWeeks, childWeeks)) &&
            (identical(other.imageUrl, imageUrl) ||
                const DeepCollectionEquality()
                    .equals(other.imageUrl, imageUrl)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(firstName) ^
      const DeepCollectionEquality().hash(lastName) ^
      const DeepCollectionEquality().hash(childYears) ^
      const DeepCollectionEquality().hash(childMonths) ^
      const DeepCollectionEquality().hash(childWeeks) ^
      const DeepCollectionEquality().hash(imageUrl);

  @JsonKey(ignore: true)
  @override
  _$UserInfoDtoCopyWith<_UserInfoDto> get copyWith =>
      __$UserInfoDtoCopyWithImpl<_UserInfoDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserInfoDtoToJson(this);
  }
}

abstract class _UserInfoDto implements UserInfoDto {
  factory _UserInfoDto(
      {@JsonKey(name: 'id') String? id,
      @JsonKey(name: 'firstName') String? firstName,
      @JsonKey(name: 'lastName') String? lastName,
      @JsonKey(name: 'childYears') int? childYears,
      @JsonKey(name: 'childMonths') int? childMonths,
      @JsonKey(name: 'childWeeks') int? childWeeks,
      @JsonKey(name: 'imageUrl') String? imageUrl}) = _$_UserInfoDto;

  factory _UserInfoDto.fromJson(Map<String, dynamic> json) =
      _$_UserInfoDto.fromJson;

  @override
  @JsonKey(name: 'id')
  String? get id => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'firstName')
  String? get firstName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'lastName')
  String? get lastName => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'childYears')
  int? get childYears => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'childMonths')
  int? get childMonths => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'childWeeks')
  int? get childWeeks => throw _privateConstructorUsedError;
  @override
  @JsonKey(name: 'imageUrl')
  String? get imageUrl => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$UserInfoDtoCopyWith<_UserInfoDto> get copyWith =>
      throw _privateConstructorUsedError;
}
