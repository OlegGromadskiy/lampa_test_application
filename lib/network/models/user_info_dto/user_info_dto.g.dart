// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserInfoDto _$_$_UserInfoDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_UserInfoDto', json, () {
    final val = _$_UserInfoDto(
      id: $checkedConvert(json, 'id', (v) => v as String?),
      firstName: $checkedConvert(json, 'firstName', (v) => v as String?),
      lastName: $checkedConvert(json, 'lastName', (v) => v as String?),
      childYears: $checkedConvert(json, 'childYears', (v) => v as int?),
      childMonths: $checkedConvert(json, 'childMonths', (v) => v as int?),
      childWeeks: $checkedConvert(json, 'childWeeks', (v) => v as int?),
      imageUrl: $checkedConvert(json, 'imageUrl', (v) => v as String?),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_UserInfoDtoToJson(_$_UserInfoDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'childYears': instance.childYears,
      'childMonths': instance.childMonths,
      'childWeeks': instance.childWeeks,
      'imageUrl': instance.imageUrl,
    };
