import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:lamp_company_tech_task/network/models/user_info_dto/user_info_dto.dart';

part 'post_dto.freezed.dart';

part 'post_dto.g.dart';

@freezed
class PostDto with _$PostDto {
  const PostDto._();

  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory PostDto({
    String? id,
    UserInfoDto? userInfo,
    DateTime? postDate,
    String? title,
    String? content,
    String? categoryName,
    String? attachedImageUrl,
    bool? isLiked,
    bool? isCommented,
    bool? isFavourite,
    int? likes,
    int? comments,
  }) = _PostDto;

  factory PostDto.fromJson(Map<String, dynamic> json) => _$PostDtoFromJson(json);
}
