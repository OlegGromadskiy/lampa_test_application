// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PostDto _$_$_PostDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_PostDto', json, () {
    final val = _$_PostDto(
      id: $checkedConvert(json, 'id', (v) => v as String?),
      userInfo: $checkedConvert(
          json,
          'user_info',
          (v) => v == null
              ? null
              : UserInfoDto.fromJson(v as Map<String, dynamic>)),
      postDate: $checkedConvert(json, 'post_date',
          (v) => v == null ? null : DateTime.parse(v as String)),
      title: $checkedConvert(json, 'title', (v) => v as String?),
      content: $checkedConvert(json, 'content', (v) => v as String?),
      categoryName: $checkedConvert(json, 'category_name', (v) => v as String?),
      attachedImageUrl:
          $checkedConvert(json, 'attached_image_url', (v) => v as String?),
      isLiked: $checkedConvert(json, 'is_liked', (v) => v as bool?),
      isCommented: $checkedConvert(json, 'is_commented', (v) => v as bool?),
      isFavourite: $checkedConvert(json, 'is_favourite', (v) => v as bool?),
      likes: $checkedConvert(json, 'likes', (v) => v as int?),
      comments: $checkedConvert(json, 'comments', (v) => v as int?),
    );
    return val;
  }, fieldKeyMap: const {
    'userInfo': 'user_info',
    'postDate': 'post_date',
    'categoryName': 'category_name',
    'attachedImageUrl': 'attached_image_url',
    'isLiked': 'is_liked',
    'isCommented': 'is_commented',
    'isFavourite': 'is_favourite'
  });
}

Map<String, dynamic> _$_$_PostDtoToJson(_$_PostDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_info': instance.userInfo?.toJson(),
      'post_date': instance.postDate?.toIso8601String(),
      'title': instance.title,
      'content': instance.content,
      'category_name': instance.categoryName,
      'attached_image_url': instance.attachedImageUrl,
      'is_liked': instance.isLiked,
      'is_commented': instance.isCommented,
      'is_favourite': instance.isFavourite,
      'likes': instance.likes,
      'comments': instance.comments,
    };
