// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'post_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PostDto _$PostDtoFromJson(Map<String, dynamic> json) {
  return _PostDto.fromJson(json);
}

/// @nodoc
class _$PostDtoTearOff {
  const _$PostDtoTearOff();

  _PostDto call(
      {String? id,
      UserInfoDto? userInfo,
      DateTime? postDate,
      String? title,
      String? content,
      String? categoryName,
      String? attachedImageUrl,
      bool? isLiked,
      bool? isCommented,
      bool? isFavourite,
      int? likes,
      int? comments}) {
    return _PostDto(
      id: id,
      userInfo: userInfo,
      postDate: postDate,
      title: title,
      content: content,
      categoryName: categoryName,
      attachedImageUrl: attachedImageUrl,
      isLiked: isLiked,
      isCommented: isCommented,
      isFavourite: isFavourite,
      likes: likes,
      comments: comments,
    );
  }

  PostDto fromJson(Map<String, Object> json) {
    return PostDto.fromJson(json);
  }
}

/// @nodoc
const $PostDto = _$PostDtoTearOff();

/// @nodoc
mixin _$PostDto {
  String? get id => throw _privateConstructorUsedError;
  UserInfoDto? get userInfo => throw _privateConstructorUsedError;
  DateTime? get postDate => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  String? get content => throw _privateConstructorUsedError;
  String? get categoryName => throw _privateConstructorUsedError;
  String? get attachedImageUrl => throw _privateConstructorUsedError;
  bool? get isLiked => throw _privateConstructorUsedError;
  bool? get isCommented => throw _privateConstructorUsedError;
  bool? get isFavourite => throw _privateConstructorUsedError;
  int? get likes => throw _privateConstructorUsedError;
  int? get comments => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostDtoCopyWith<PostDto> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostDtoCopyWith<$Res> {
  factory $PostDtoCopyWith(PostDto value, $Res Function(PostDto) then) =
      _$PostDtoCopyWithImpl<$Res>;
  $Res call(
      {String? id,
      UserInfoDto? userInfo,
      DateTime? postDate,
      String? title,
      String? content,
      String? categoryName,
      String? attachedImageUrl,
      bool? isLiked,
      bool? isCommented,
      bool? isFavourite,
      int? likes,
      int? comments});

  $UserInfoDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class _$PostDtoCopyWithImpl<$Res> implements $PostDtoCopyWith<$Res> {
  _$PostDtoCopyWithImpl(this._value, this._then);

  final PostDto _value;
  // ignore: unused_field
  final $Res Function(PostDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? userInfo = freezed,
    Object? postDate = freezed,
    Object? title = freezed,
    Object? content = freezed,
    Object? categoryName = freezed,
    Object? attachedImageUrl = freezed,
    Object? isLiked = freezed,
    Object? isCommented = freezed,
    Object? isFavourite = freezed,
    Object? likes = freezed,
    Object? comments = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as UserInfoDto?,
      postDate: postDate == freezed
          ? _value.postDate
          : postDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
      categoryName: categoryName == freezed
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      attachedImageUrl: attachedImageUrl == freezed
          ? _value.attachedImageUrl
          : attachedImageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      isLiked: isLiked == freezed
          ? _value.isLiked
          : isLiked // ignore: cast_nullable_to_non_nullable
              as bool?,
      isCommented: isCommented == freezed
          ? _value.isCommented
          : isCommented // ignore: cast_nullable_to_non_nullable
              as bool?,
      isFavourite: isFavourite == freezed
          ? _value.isFavourite
          : isFavourite // ignore: cast_nullable_to_non_nullable
              as bool?,
      likes: likes == freezed
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int?,
      comments: comments == freezed
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }

  @override
  $UserInfoDtoCopyWith<$Res>? get userInfo {
    if (_value.userInfo == null) {
      return null;
    }

    return $UserInfoDtoCopyWith<$Res>(_value.userInfo!, (value) {
      return _then(_value.copyWith(userInfo: value));
    });
  }
}

/// @nodoc
abstract class _$PostDtoCopyWith<$Res> implements $PostDtoCopyWith<$Res> {
  factory _$PostDtoCopyWith(_PostDto value, $Res Function(_PostDto) then) =
      __$PostDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? id,
      UserInfoDto? userInfo,
      DateTime? postDate,
      String? title,
      String? content,
      String? categoryName,
      String? attachedImageUrl,
      bool? isLiked,
      bool? isCommented,
      bool? isFavourite,
      int? likes,
      int? comments});

  @override
  $UserInfoDtoCopyWith<$Res>? get userInfo;
}

/// @nodoc
class __$PostDtoCopyWithImpl<$Res> extends _$PostDtoCopyWithImpl<$Res>
    implements _$PostDtoCopyWith<$Res> {
  __$PostDtoCopyWithImpl(_PostDto _value, $Res Function(_PostDto) _then)
      : super(_value, (v) => _then(v as _PostDto));

  @override
  _PostDto get _value => super._value as _PostDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? userInfo = freezed,
    Object? postDate = freezed,
    Object? title = freezed,
    Object? content = freezed,
    Object? categoryName = freezed,
    Object? attachedImageUrl = freezed,
    Object? isLiked = freezed,
    Object? isCommented = freezed,
    Object? isFavourite = freezed,
    Object? likes = freezed,
    Object? comments = freezed,
  }) {
    return _then(_PostDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      userInfo: userInfo == freezed
          ? _value.userInfo
          : userInfo // ignore: cast_nullable_to_non_nullable
              as UserInfoDto?,
      postDate: postDate == freezed
          ? _value.postDate
          : postDate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
      categoryName: categoryName == freezed
          ? _value.categoryName
          : categoryName // ignore: cast_nullable_to_non_nullable
              as String?,
      attachedImageUrl: attachedImageUrl == freezed
          ? _value.attachedImageUrl
          : attachedImageUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      isLiked: isLiked == freezed
          ? _value.isLiked
          : isLiked // ignore: cast_nullable_to_non_nullable
              as bool?,
      isCommented: isCommented == freezed
          ? _value.isCommented
          : isCommented // ignore: cast_nullable_to_non_nullable
              as bool?,
      isFavourite: isFavourite == freezed
          ? _value.isFavourite
          : isFavourite // ignore: cast_nullable_to_non_nullable
              as bool?,
      likes: likes == freezed
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int?,
      comments: comments == freezed
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)
class _$_PostDto extends _PostDto {
  const _$_PostDto(
      {this.id,
      this.userInfo,
      this.postDate,
      this.title,
      this.content,
      this.categoryName,
      this.attachedImageUrl,
      this.isLiked,
      this.isCommented,
      this.isFavourite,
      this.likes,
      this.comments})
      : super._();

  factory _$_PostDto.fromJson(Map<String, dynamic> json) =>
      _$_$_PostDtoFromJson(json);

  @override
  final String? id;
  @override
  final UserInfoDto? userInfo;
  @override
  final DateTime? postDate;
  @override
  final String? title;
  @override
  final String? content;
  @override
  final String? categoryName;
  @override
  final String? attachedImageUrl;
  @override
  final bool? isLiked;
  @override
  final bool? isCommented;
  @override
  final bool? isFavourite;
  @override
  final int? likes;
  @override
  final int? comments;

  @override
  String toString() {
    return 'PostDto(id: $id, userInfo: $userInfo, postDate: $postDate, title: $title, content: $content, categoryName: $categoryName, attachedImageUrl: $attachedImageUrl, isLiked: $isLiked, isCommented: $isCommented, isFavourite: $isFavourite, likes: $likes, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PostDto &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.userInfo, userInfo) ||
                const DeepCollectionEquality()
                    .equals(other.userInfo, userInfo)) &&
            (identical(other.postDate, postDate) ||
                const DeepCollectionEquality()
                    .equals(other.postDate, postDate)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.content, content) ||
                const DeepCollectionEquality()
                    .equals(other.content, content)) &&
            (identical(other.categoryName, categoryName) ||
                const DeepCollectionEquality()
                    .equals(other.categoryName, categoryName)) &&
            (identical(other.attachedImageUrl, attachedImageUrl) ||
                const DeepCollectionEquality()
                    .equals(other.attachedImageUrl, attachedImageUrl)) &&
            (identical(other.isLiked, isLiked) ||
                const DeepCollectionEquality()
                    .equals(other.isLiked, isLiked)) &&
            (identical(other.isCommented, isCommented) ||
                const DeepCollectionEquality()
                    .equals(other.isCommented, isCommented)) &&
            (identical(other.isFavourite, isFavourite) ||
                const DeepCollectionEquality()
                    .equals(other.isFavourite, isFavourite)) &&
            (identical(other.likes, likes) ||
                const DeepCollectionEquality().equals(other.likes, likes)) &&
            (identical(other.comments, comments) ||
                const DeepCollectionEquality()
                    .equals(other.comments, comments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(userInfo) ^
      const DeepCollectionEquality().hash(postDate) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(content) ^
      const DeepCollectionEquality().hash(categoryName) ^
      const DeepCollectionEquality().hash(attachedImageUrl) ^
      const DeepCollectionEquality().hash(isLiked) ^
      const DeepCollectionEquality().hash(isCommented) ^
      const DeepCollectionEquality().hash(isFavourite) ^
      const DeepCollectionEquality().hash(likes) ^
      const DeepCollectionEquality().hash(comments);

  @JsonKey(ignore: true)
  @override
  _$PostDtoCopyWith<_PostDto> get copyWith =>
      __$PostDtoCopyWithImpl<_PostDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_PostDtoToJson(this);
  }
}

abstract class _PostDto extends PostDto {
  const factory _PostDto(
      {String? id,
      UserInfoDto? userInfo,
      DateTime? postDate,
      String? title,
      String? content,
      String? categoryName,
      String? attachedImageUrl,
      bool? isLiked,
      bool? isCommented,
      bool? isFavourite,
      int? likes,
      int? comments}) = _$_PostDto;
  const _PostDto._() : super._();

  factory _PostDto.fromJson(Map<String, dynamic> json) = _$_PostDto.fromJson;

  @override
  String? get id => throw _privateConstructorUsedError;
  @override
  UserInfoDto? get userInfo => throw _privateConstructorUsedError;
  @override
  DateTime? get postDate => throw _privateConstructorUsedError;
  @override
  String? get title => throw _privateConstructorUsedError;
  @override
  String? get content => throw _privateConstructorUsedError;
  @override
  String? get categoryName => throw _privateConstructorUsedError;
  @override
  String? get attachedImageUrl => throw _privateConstructorUsedError;
  @override
  bool? get isLiked => throw _privateConstructorUsedError;
  @override
  bool? get isCommented => throw _privateConstructorUsedError;
  @override
  bool? get isFavourite => throw _privateConstructorUsedError;
  @override
  int? get likes => throw _privateConstructorUsedError;
  @override
  int? get comments => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PostDtoCopyWith<_PostDto> get copyWith =>
      throw _privateConstructorUsedError;
}
