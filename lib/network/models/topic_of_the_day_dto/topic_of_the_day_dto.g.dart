// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'topic_of_the_day_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TopicOfTheDayDto _$_$_TopicOfTheDayDtoFromJson(Map<String, dynamic> json) {
  return $checkedNew(r'_$_TopicOfTheDayDto', json, () {
    final val = _$_TopicOfTheDayDto(
      content: $checkedConvert(json, 'content', (v) => v as String?),
    );
    return val;
  });
}

Map<String, dynamic> _$_$_TopicOfTheDayDtoToJson(
        _$_TopicOfTheDayDto instance) =>
    <String, dynamic>{
      'content': instance.content,
    };
