// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'topic_of_the_day_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TopicOfTheDayDto _$TopicOfTheDayDtoFromJson(Map<String, dynamic> json) {
  return _TopicOfTheDayDto.fromJson(json);
}

/// @nodoc
class _$TopicOfTheDayDtoTearOff {
  const _$TopicOfTheDayDtoTearOff();

  _TopicOfTheDayDto call({String? content}) {
    return _TopicOfTheDayDto(
      content: content,
    );
  }

  TopicOfTheDayDto fromJson(Map<String, Object> json) {
    return TopicOfTheDayDto.fromJson(json);
  }
}

/// @nodoc
const $TopicOfTheDayDto = _$TopicOfTheDayDtoTearOff();

/// @nodoc
mixin _$TopicOfTheDayDto {
  String? get content => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TopicOfTheDayDtoCopyWith<TopicOfTheDayDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TopicOfTheDayDtoCopyWith<$Res> {
  factory $TopicOfTheDayDtoCopyWith(
          TopicOfTheDayDto value, $Res Function(TopicOfTheDayDto) then) =
      _$TopicOfTheDayDtoCopyWithImpl<$Res>;
  $Res call({String? content});
}

/// @nodoc
class _$TopicOfTheDayDtoCopyWithImpl<$Res>
    implements $TopicOfTheDayDtoCopyWith<$Res> {
  _$TopicOfTheDayDtoCopyWithImpl(this._value, this._then);

  final TopicOfTheDayDto _value;
  // ignore: unused_field
  final $Res Function(TopicOfTheDayDto) _then;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_value.copyWith(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$TopicOfTheDayDtoCopyWith<$Res>
    implements $TopicOfTheDayDtoCopyWith<$Res> {
  factory _$TopicOfTheDayDtoCopyWith(
          _TopicOfTheDayDto value, $Res Function(_TopicOfTheDayDto) then) =
      __$TopicOfTheDayDtoCopyWithImpl<$Res>;
  @override
  $Res call({String? content});
}

/// @nodoc
class __$TopicOfTheDayDtoCopyWithImpl<$Res>
    extends _$TopicOfTheDayDtoCopyWithImpl<$Res>
    implements _$TopicOfTheDayDtoCopyWith<$Res> {
  __$TopicOfTheDayDtoCopyWithImpl(
      _TopicOfTheDayDto _value, $Res Function(_TopicOfTheDayDto) _then)
      : super(_value, (v) => _then(v as _TopicOfTheDayDto));

  @override
  _TopicOfTheDayDto get _value => super._value as _TopicOfTheDayDto;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_TopicOfTheDayDto(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(
    fieldRename: FieldRename.snake, checked: true, explicitToJson: true)
class _$_TopicOfTheDayDto extends _TopicOfTheDayDto {
  const _$_TopicOfTheDayDto({this.content}) : super._();

  factory _$_TopicOfTheDayDto.fromJson(Map<String, dynamic> json) =>
      _$_$_TopicOfTheDayDtoFromJson(json);

  @override
  final String? content;

  @override
  String toString() {
    return 'TopicOfTheDayDto(content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _TopicOfTheDayDto &&
            (identical(other.content, content) ||
                const DeepCollectionEquality().equals(other.content, content)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(content);

  @JsonKey(ignore: true)
  @override
  _$TopicOfTheDayDtoCopyWith<_TopicOfTheDayDto> get copyWith =>
      __$TopicOfTheDayDtoCopyWithImpl<_TopicOfTheDayDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_TopicOfTheDayDtoToJson(this);
  }
}

abstract class _TopicOfTheDayDto extends TopicOfTheDayDto {
  const factory _TopicOfTheDayDto({String? content}) = _$_TopicOfTheDayDto;
  const _TopicOfTheDayDto._() : super._();

  factory _TopicOfTheDayDto.fromJson(Map<String, dynamic> json) =
      _$_TopicOfTheDayDto.fromJson;

  @override
  String? get content => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$TopicOfTheDayDtoCopyWith<_TopicOfTheDayDto> get copyWith =>
      throw _privateConstructorUsedError;
}
