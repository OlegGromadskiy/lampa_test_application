import 'package:freezed_annotation/freezed_annotation.dart';

part 'topic_of_the_day_dto.freezed.dart';

part 'topic_of_the_day_dto.g.dart';

@freezed
class TopicOfTheDayDto with _$TopicOfTheDayDto {
  const TopicOfTheDayDto._();

  @JsonSerializable(
    fieldRename: FieldRename.snake,
    checked: true,
    explicitToJson: true,
  )
  const factory TopicOfTheDayDto({
    String? content,
  }) = _TopicOfTheDayDto;

  factory TopicOfTheDayDto.fromJson(Map<String, dynamic> json) => _$TopicOfTheDayDtoFromJson(json);
}
