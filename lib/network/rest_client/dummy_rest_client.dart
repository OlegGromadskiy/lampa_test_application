part of 'rest_client.dart';

class _DummyRestClient implements RestClient {
  final Faker faker = Faker();

  _DummyRestClient(Dio dio, {String? baseUrl});

  @override
  Future<List<PostDto>> fetchPosts(queries) async {
    await Future.delayed(Duration(seconds: 1));

    return List.generate(
      20,
      (index) {
        return PostDto(
          userInfo: UserInfoDto(
            id: faker.guid.guid(),
            imageUrl: faker.image.image(keywords: ['woman', 'woman_avatar', 'woman_portrait'], random: true),
            lastName: faker.person.lastName(),
            firstName: faker.person.firstName(),
            childYears: faker.randomGenerator.numbers(2, 1).single,
            childMonths: faker.randomGenerator.numbers(13, 1).single,
            childWeeks: faker.randomGenerator.numbers(5, 1).single,
          ),
          id: faker.guid.guid(),
          attachedImageUrl: faker.randomGenerator.boolean() && faker.randomGenerator.boolean()
              ? faker.image.image(keywords: ['baby', 'baby_with_mom'], random: true)
              : null,
          postDate: faker.date.dateTime(minYear: 2021, maxYear: 2021),
          likes: faker.randomGenerator.numbers(99, 1).single,
          comments: faker.randomGenerator.numbers(99, 1).single,
          content: faker.lorem.sentences(10).join(),
          title: faker.lorem.sentence(),
          categoryName: faker.lorem.word(),
          isCommented: faker.randomGenerator.boolean(),
          isFavourite: faker.randomGenerator.boolean(),
          isLiked: faker.randomGenerator.boolean(),
        );
      },
    );
  }

  @override
  Future<TopicOfTheDayDto> fetchTopic() async {
    await Future.delayed(Duration(seconds: 1));

    return TopicOfTheDayDto(
      content: faker.lorem.sentence(),
    );
  }
}
