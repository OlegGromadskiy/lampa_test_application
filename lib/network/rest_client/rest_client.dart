import 'package:dio/dio.dart';
import 'package:faker/faker.dart';
import 'package:flutter/foundation.dart';
import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';
import 'package:lamp_company_tech_task/network/models/user_info_dto/user_info_dto.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';
part 'dummy_rest_client.dart';

@RestApi(baseUrl: 'https://base_url')
abstract class RestClient{
  factory RestClient(Dio dio, {String baseUrl}) = _DummyRestClient;

  static final dio = Dio();

  @GET('/posts')
  Future<List<PostDto>> fetchPosts(@Queries() queries);

  @GET('/topic')
  Future<TopicOfTheDayDto> fetchTopic();
}