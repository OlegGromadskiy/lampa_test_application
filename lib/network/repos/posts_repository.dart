import 'package:lamp_company_tech_task/network/models/post_dto/post_dto.dart';
import 'package:lamp_company_tech_task/network/models/topic_of_the_day_dto/topic_of_the_day_dto.dart';
import 'package:lamp_company_tech_task/network/rest_client/rest_client.dart';

abstract class PostsRepository{
  PostsRepository._();
  static final _restClient = RestClient(RestClient.dio);

  static Future<List<PostDto>> fetchPosts(int offset, int count){
    final result = _restClient.fetchPosts({'offset':offset, 'count' : count});

    return result;
  }

  static Future<TopicOfTheDayDto> fetchTopic(){
    final result = _restClient.fetchTopic();

    return result;
  }
}